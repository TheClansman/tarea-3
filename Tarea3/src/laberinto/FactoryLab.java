package laberinto;

import buscadorpalabras.IndividualPal;
import framework.IFactory;

public class FactoryLab extends IFactory<Road>{
	
	/**
	 * Overrides createIndividual(Object a) returning null because it isn't used.
	 */
	@Override
	public Road createIndividual(Object a){
		return null;
	}
	
	/**
	 * Creates a new Road using salida, jugador and lab.
	 * @param salida
	 * @param jugador
	 * @param lab
	 * @return	new Road.
	 */
	public Road createIndividual(Cell salida, Cell jugador, Laberinto lab) {
		return new Road(salida, jugador, lab);
	}

}
