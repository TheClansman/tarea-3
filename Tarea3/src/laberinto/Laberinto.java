package laberinto;

public class Laberinto {
	
	private Cell[][] matrix;
	double wallRate = 0.1;
	protected int N;
	protected int M;
	
	/**
	 * Public constructor for Laberinto, then uses generateLaberinto to create the cells.
	 * @param n
	 * @param m
	 */
	public Laberinto(int n, int m){
		matrix = new Cell[n][m];
		this.N=n;
		this.M=m;
		generateLaberinto();
	}
	
	/**
	 * Generates a random Labyrinth using Math.random() with a wall rate, then creates one exit and one player at random.
	 */
	public void generateLaberinto(){
		boolean exitDone=false;
		boolean playerDone=false;
		for(int i=0; i<N; i++){
			for(int j=0; j<M; j++){
				if(Math.random()<wallRate){
					matrix[i][j]=new Wall(i, j);
				}
				else{
					matrix[i][j]=new Floor(i, j);
				}
			}
		}
		int x;
		int y;
		int j;
		int k;
		while(true){
			x=(int)(Math.random()*N);
			y=(int)(Math.random()*M);
			Cell a=matrix[x][y];
			if(a.isFloor()){
				a=new Exit(x, y);
				break;}
		}
		while(true){
			j=(int)(Math.random()*N);
			k=(int)(Math.random()*M);
			Cell b=matrix[j][k];
			if(b.isFloor()){
				b=new Player(j, k);
				break;}
		}
			
	}
	
	/**
	 * Returns the cell in the matrix[x][y]
	 * @param x
	 * @param y
	 * @return
	 */
	public Cell getCell(int x, int y){
		return matrix[x][y];
	}
	
	/**
	 * Sets the cell to the Cell in the matrix[x][y]
	 * @param x
	 * @param y
	 * @param cell
	 */
	public void setCell(int x, int y, Cell cell){
		matrix[x][y]=cell;
	}
	
	/**
	 * Returns the protected variable N.
	 */
	public int getN(){
		return N;
	}

	/**
	 * Returns the protected variable M.
	 * @return
	 */
	public int getM(){
		return M;
	}
}
