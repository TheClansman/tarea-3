package laberinto;

public abstract class Cell{
	
	private int x;
	private int y;
	protected String color;
	
	/**
	 * Public constructor for Cell.
	 * @param x	x axis
	 * @param y	y axis
	 */
	public Cell(int x, int y){
		this.x=x;
		this.y=y;
	}
	
	/**
	 * By default returns false since only one kind of Cell is a Wall.
	 * @return
	 */
	public boolean isWall(){
		return false;
	}
	
	/**
	 * By default returns false since only one kind of Cell is a Floor.
	 * @return
	 */
	public boolean isFloor(){
		return false;
	}
	
	/**
	 * By default returns false since only one kind of Cell is an Exit.
	 * @return
	 */
	public boolean isExit(){
		return false;
	}
	
	/**
	 * By default returns false since only one kind of Cell is a Player.
	 * @return
	 */
	public boolean isPlayer(){
		return false;
	}
	
	/**
	 * Returns the protected variable color.
	 * @return
	 */
	public String getColor(){
		return color;
	}
	
	/**
	 * Sets the protected variable color to _color.
	 * @param _color
	 */
	public void setColor(String _color){
		this.color=_color;
	}
	
	/**
	 * Returns the protected variable X (x axis).
	 * @return
	 */
	public int getX(){
		return x;
	}
	
	/**
	 * Returns the protected variable Y (y axis).
	 * @return
	 */
	public int getY(){
		return y;
	}

}
