package laberinto;

public class Wall extends Cell{

	/**
	 * Public constructor for Wall.
	 * @param x	x axis.
	 * @param y y axis.
	 */
	public Wall(int x, int y) {
		super(x, y);
		this.color="black";
	}
	
	/**
	 * Overrides the function isWall() returning True.
	 */
	@Override
	public boolean isWall(){
		return true;
	}

}
