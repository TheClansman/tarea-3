package laberinto;

import java.util.ArrayList;

import buscadorpalabras.IndividualPal;
import buscadorpalabras.PopulationPal;
import framework.IIndividual;
import framework.IPopulation;

public class PopulationLab extends IPopulation<Road> {
	
	private final int TOURNAMENT_SIZE = 5;
	private Cell salida;
	private Cell jugador;
	private Laberinto lab;
	private FactoryLab factory;
	
	/**
	 * Public constructor for PopulationLab.
	 * @param salida
	 * @param jugador
	 * @param lab
	 */
	public PopulationLab(Cell salida, Cell jugador, Laberinto lab){
		this.salida=salida;
		this.jugador=jugador;
		this.lab=lab;
		this.factory = new FactoryLab();
		
	}

	/**
	 * Creates individuals for the populationlab using the FactoryLab.
	 */
	@Override
	public void create(int size) {
		individuals = new ArrayList<Road>();
		for(int i = 0; i < size; i++)
			individuals.add(factory.createIndividual(salida, jugador, lab));
	}

	/**
	 * Evolves the population by creating a new one, using elitism selects the fittest, crossover them, mutates them and returns the new population. 
	 */
	@Override
	public IPopulation evolve() {
		PopulationLab newPopulation = new PopulationLab(salida, jugador, lab);
		newPopulation.create(this.numberOfIndividuals());
		if(this.shouldUseElistism()) 
			newPopulation.individualAtPut(1, (Road) this.fittestIndividual());
		
		int elitismOffset = this.shouldUseElistism()?1:0;
		
		//Loop iver the population size and create new individual with crossover
		for(int index = elitismOffset; index < this.numberOfIndividuals(); index++) {
			Road newIndividual = (Road) this.tournamentSelection().crossOverWith(this.tournamentSelection());
			newPopulation.individualAtPut(index, newIndividual);
		}
		
		//Mutate population
		for(int index = elitismOffset; index < this.numberOfIndividuals(); index++)
			newPopulation.individualAt(index).mutate();

		return newPopulation;
	}

	/**
	 * Selects a new population of individuals and returns the fittest one from that population.
	 */
	@Override
	public IIndividual tournamentSelection() {
		PopulationLab newPopulation = new PopulationLab(salida, jugador, lab);
		newPopulation.create(this.tournamentSize());
		for(int i = 0; i < this.tournamentSize(); i++) {
			int randomIndex = (int)(Math.random()*this.numberOfIndividuals());
			newPopulation.individualAtPut(i, (Road) (this.individualAt(randomIndex)));
		}
		return newPopulation.fittestIndividual();
	}

	/**
	 * Returns true for using elitism.
	 */
	@Override
	public boolean shouldUseElistism() {
		return true;
	}
	
	/**
	 * Returns the private variable TOURNAMENT_SIZE.
	 * @return
	 */
	public int tournamentSize(){
		return TOURNAMENT_SIZE;
	}

}
