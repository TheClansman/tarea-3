package laberinto;

public class Floor extends Cell {

	/**
	 * Public constructor for Floor.
	 * @param x	x axis
	 * @param y	y axis
	 */
	public Floor(int x, int y) {
		super(x, y);
	}
	
	/**
	 * Overrides the function isFloor returning True.
	 */
	@Override
	public boolean isFloor(){
		return true;
	}

}
