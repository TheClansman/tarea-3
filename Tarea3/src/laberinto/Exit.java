package laberinto;

public class Exit extends Cell{

	/**
	 * Public constructor for Exit.
	 * @param x
	 * @param y
	 */
	public Exit(int x, int y) {
		super(x, y);
		// TODO Auto-generated constructor stub
	}

	/**
	 * Overrides the function isExit returning True.
	 */
	@Override
	public boolean isExit(){
		return true;
	}
}
