package laberinto;

public class East extends Direction {
	
	/**
	 * Returns an array with the same values but with int[0]+1.
	 */
	public int[] getDir(int x, int y){
		int[] retorno = new int[2];
		retorno[0]=x+1;
		retorno[1]=y;
		return retorno;
	}
}