package laberinto;

public class South extends Direction {
	
	
	/**
	 * Returns an array with the same values but with int[1]-1.
	 */
	public int[] getDir(int x, int y){
		int[] retorno = new int[2];
		retorno[0]=x;
		retorno[1]=y-1;
		return retorno;
	}
}
