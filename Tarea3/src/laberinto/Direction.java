package laberinto;

public abstract class Direction{
	
	/**
	 * Abstract function, get Direction from that movement.
	 * @param x
	 * @param y
	 * @return
	 */
	public abstract int[] getDir(int x, int y);

}
