package laberinto;

public class Player extends Cell {

	/**
	 * Public constructor for Player.
	 * @param x	x axis
	 * @param y	y axis
	 */
	public Player(int x, int y) {
		super(x, y);
	}
	
	/**
	 * Overrides the function isPlayer returning True.
	 */
	@Override
	public boolean isPlayer(){
		return true;
	}

}
