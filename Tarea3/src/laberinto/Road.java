package laberinto;
import java.util.ArrayList;
import java.util.List;

import framework.IIndividual;


public class Road extends IIndividual<Direction>{
	
	private Cell salida;
	private Cell jugador;
	private Laberinto lab;
	
	/**
	 * Public constructor for Road, it requires an exit, a player and a labyrinth.
	 * @param salida
	 * @param jugador
	 * @param lab
	 */
	public Road(Cell salida, Cell jugador, Laberinto lab){
		genes=new ArrayList<Direction>();
		this.salida=salida;
		this.jugador=jugador;
		this.lab=lab;
		this.mutationRate = 0.015;
		this.uniformRate = 0.5;
		numberOfGenes=0;
		genes=this.generateGenes();
	}

	/**
	 * Crossover individuals function, using their genes at random to create a new individual.
	 */
	@Override
	public IIndividual<Direction> crossOverWith(IIndividual<Direction> individual	) {
		Road newIndividual = new Road(salida, jugador, lab);
		for(int index = 0; index < this.numberOfGenes; index++){
			if(Math.random() < uniformRate ){
				newIndividual.geneAtPut(index, (Direction) this.geneAt(index));}				
			else{
				newIndividual.geneAtPut(index, (Direction) individual.geneAt(index));}
		}
		return newIndividual;
	}

	/**
	 * Mutates the individual by randomizing some of its genes to new ones according to the mutationRate.
	 */
	@Override
	public void mutate() {
		double index;
		for(int i = 0; i < this.numberOfGenes; i++)
			if(Math.random() < this.mutationRate){
				index=Math.random();
				if(index<0.25){
					this.geneAtPut(i, new North());
					continue;
				}
				else if(index<0.5){
					this.geneAtPut(i, new South());
					continue;
				}
				else if(index<0.75){
					this.geneAtPut(i, new East());
					continue;
				}
				else{
					this.geneAtPut(i, new West());
					continue;
				}
				
			}
		
	}
	
	/**
	 * Gets the cell at the end of a road by following all the steps in the genes.
	 * @return
	 */
	public Cell getCurrentCell(){
		int[] index = new int[2];
		Cell cellIndex = jugador;
		for(int i=0; i<numberOfGenes; i++){
			index=((Direction)this.geneAt(i)).getDir(cellIndex.getX(), cellIndex.getY());
			cellIndex = lab.getCell(index[0], index[1]);
		}
		return cellIndex;
	}
	
	/**
	 * Generates all the genes for the individual (Road), it makes sure that all new genes added are next to the previous one so all the individuals are connected Roads.
	 */
	@Override
	public List<Direction> generateGenes() {
		List<Direction> answer = new ArrayList<Direction>();
		double random;
		while(true){
			random=Math.random();
			if(random<0.25 && getCurrentCell().getY()+1<lab.getN()){
					if(!lab.getCell(getCurrentCell().getX(), getCurrentCell().getY()+1).isWall()){
						answer.add(new North());
						numberOfGenes++;
						if(getCurrentCell().equals(salida)){
							break;
						}
						continue;}
			}
			else if(random<0.5 && getCurrentCell().getY()-1>=0){
				if(!lab.getCell(getCurrentCell().getX(), getCurrentCell().getY()-1).isWall()){
					answer.add(new South());
					numberOfGenes++;
					if(getCurrentCell().equals(salida)){
						break;
					}
					continue;
				}
			}
			else if(random<0.75 && getCurrentCell().getX()+1<lab.getM()){
				if(!lab.getCell(getCurrentCell().getX()+1, getCurrentCell().getY()).isWall()){
					answer.add(new East());
					numberOfGenes++;
					if(getCurrentCell().equals(salida)){
						break;
					}
					continue;
				}
			}
			else if(random<1 && getCurrentCell().getX()-1>=0){
				if(!lab.getCell(getCurrentCell().getX()-1, getCurrentCell().getY()).isWall()){
					answer.add(new West());
					numberOfGenes++;
					if(getCurrentCell().equals(salida)){
						break;
					}
					continue;
				}
			}
		}
		return answer;
	}
	
	/**
	 * Calculates the fitness of a Road by the proximity to the exit and the number of steps it uses to get there. The more score, the better the fit.
	 */
	@Override
	public int fitness(){
		int score = 0;
		score+=(lab.getN()-(Math.abs(getCurrentCell().getX() - salida.getX())) + lab.getM()- (Math.abs(getCurrentCell().getY() - salida.getY())));
		score-=numberOfGenes;
		return score;
	}

}
