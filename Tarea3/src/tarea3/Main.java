package tarea3;
import java.util.ArrayList;
import java.util.List;
import buscadorpalabras.PopulationPal;



public class Main {
	
	public static void main(String[] argv) {
		PopulationPal pop = new PopulationPal("hola");
		pop.createAndGenerateIndividual(50);
		
		for(int loop = 0; loop < 100; loop++) {
			System.out.println(pop.fittestIndividual().fitness());
			pop = (PopulationPal) pop.evolve();
		}
		System.out.println(pop.fittestIndividual().genesAsString());
		
	}
}
