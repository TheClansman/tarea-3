package tarea3;
import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import buscadorpalabras.*;
import framework.*;
import laberinto.*;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.junit.Assert.assertThat;




public class Tests {
	
	PopulationPal pop;
	Cell floor;
	Cell wall;
	Cell player;
	Cell exit;
	IFactory<Road> factorylab;
	IFactory<IndividualPal> factorypal;
	Laberinto lab;
	Direction north;
	Direction south;
	Direction east;
	Direction west;
	PopulationLab pop2;

	/**
	 * Creates a PopulationPal whose goal is to find the string "h"
	 */
	@Before
	public void setUp(){
	pop = new PopulationPal("h");
	}
	
	/**
	 * Finds the String "h" with 500 individuals and 500 iterations.
	 */
	@Test
	public void testEncuentraPalabras(){
		pop.createAndGenerateIndividual(100);
		for(int loop = 0; loop < 500; loop++) {
			pop = (PopulationPal) pop.evolve();
		}
		assertEquals(pop.fittestIndividual().genesAsString(), "h");
	}
	
	/**
	 * Tests the getters giving the correct values according to the type of Individual.
	 */
	@Test
	public void testGets(){
		IFactory<IndividualPal> Factory = new FactoryPal();
		IndividualPal In = Factory.createIndividual("ho");
		assertTrue(0.5==In.getUniformRate());
		assertTrue(0.015==In.getMutationRate());
		assertTrue(2==In.getNumberOfGenes());
		
	}
	
	/**
	 * Creates instances of the variables floor, wall, player and exit.
	 */
	@Before
	public void setUp2(){
		floor=new Floor(1, 3);
		wall=new Wall(2, 3);
		player=new Player(3, 4);
		exit=new Exit(1, 2);
		
	}
	
	/**
	 * Test those instances by using the functions getX(), getY(), and isWall(), isFloor(), isPlayer() and isExit().
	 */
	@Test
	public void testTypes(){
		assertEquals(floor.getX(), 1);
		assertEquals(floor.getY(), 3);
		assertTrue(floor.isFloor());
		assertTrue(!floor.isWall());
		assertTrue(!floor.isPlayer());
		assertTrue(!floor.isExit());
		assertEquals(wall.getX(), 2);
		assertEquals(wall.getY(), 3);
		assertTrue(!wall.isFloor());
		assertTrue(wall.isWall());
		assertTrue(!wall.isPlayer());
		assertTrue(!wall.isExit());
		assertEquals(player.getX(), 3);
		assertEquals(player.getY(), 4);
		assertTrue(!player.isFloor());
		assertTrue(!player.isWall());
		assertTrue(player.isPlayer());
		assertTrue(!player.isExit());
		assertEquals(exit.getX(), 1);
		assertEquals(exit.getY(), 2);
		assertTrue(!exit.isFloor());
		assertTrue(!exit.isWall());
		assertTrue(!exit.isPlayer());
		assertTrue(exit.isExit());
	}
	
	/**
	 * Sets the color of floor to red, check if it's red.
	 */
	@Test
	public void testColor(){
		floor.setColor("red");
		assertEquals("red", floor.getColor());
	}
	
	/**
	 * Creates instances of the variables Laberinto lab and the factories FactoryLab factorylab and FactoryPal factoryPal.
	 */
	@Before
	public void setUp3(){
		lab = new Laberinto(10, 10);
		factorylab = new FactoryLab();
		factorypal = new FactoryPal();
	}
	
	/**
	 * Tests the types returned from the factories.
	 */
	@Test
	public void testFactory(){
		IIndividual<Direction> c = factorylab.createIndividual(new Object());
		//IIndividual<Direction> a = factorylab.createIndividual(exit, player, lab);
		IIndividual<String> b = factorypal.createIndividual("hola");
		assertTrue(c==null);
		assertThat(b, instanceOf(IndividualPal.class));
		//assertThat(a, instanceOf(Road.class));
		assertThat(lab.getCell(3, 3), instanceOf(Cell.class));

	}
	
	/**
	 * Creates instances for the variables North north, South south, East east and West west.
	 */
	@Before
	public void setUp4(){
		north = new North();
		south = new South();
		east = new East();
		west = new West();
	}
	
	/**
	 * Checks the coordenates for the variables north, south, east and west after applying the step in the road.
	 */
	@Test
	public void testDirections(){
		int[] index=north.getDir(1,2);
		assertTrue(north.getDir(1, 2)[0] == 1);
		assertTrue(north.getDir(1, 2)[1] == 3);
		assertTrue(south.getDir(2, 2)[0] == 2);
		assertTrue(south.getDir(2, 2)[1] == 1);
		assertTrue(east.getDir(3, 2)[0] == 4);
		assertTrue(east.getDir(3, 2)[1] == 2);
		assertTrue(west.getDir(4, 2)[0] == 3);
		assertTrue(west.getDir(4, 2)[1] == 2);
	}
	
	/**
	 * Creates a PopulationLab.
	 */
	@Before
	public void setUp5(){
		pop2 = new PopulationLab(exit, player, lab);
	}
	
	/**
	 * Checks if shouldUseelitism is returning true.
	 */
	@Test
	public void testLaberinto(){
		assertTrue(pop2.shouldUseElistism());
	}

	
		
}
