package buscadorpalabras;

import framework.IFactory;

public class FactoryPal extends IFactory<IndividualPal> {

	
	/**
	 * Creates a new IndividualPal using the String Palabra.
	 */
	@Override
	public IndividualPal createIndividual(Object Palabra) {
		return new IndividualPal((String)Palabra);
	}

}
