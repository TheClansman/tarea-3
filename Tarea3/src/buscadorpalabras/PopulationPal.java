package buscadorpalabras;
import java.util.ArrayList;

import framework.IIndividual;
import framework.IPopulation;


public class PopulationPal extends IPopulation<IndividualPal> {
	
	private String Palabra;
	private FactoryPal factory;
	private final int TOURNAMENT_SIZE = 5;
	
	/**
	 * Constructor, uses String Palabra as the goal to find.
	 * @param Palabra String, word that the algorithm is looking to find.
	 */
	public PopulationPal(String Palabra){
		this.factory = new FactoryPal();
		this.Palabra = Palabra;
	}

	/**
	 * Creates "size" individuals using a FactoryPal.
	 */
	@Override
	public void create(int size) {
		individuals = new ArrayList<IndividualPal>();
		for(int i = 0; i < size; i++)
			individuals.add(factory.createIndividual(Palabra));
		
	}
	
	/**
	 * Evolves the individuals, first it creates a new population, then find the fittest individual of that population, create a new selection, fuse them, mutate them, and returns
	 * the new population.
	 */
	@Override
	public IPopulation evolve() {
		PopulationPal newPopulation = new PopulationPal(Palabra);
		newPopulation.create(this.numberOfIndividuals());
		if(this.shouldUseElistism()) 
			newPopulation.individualAtPut(1, (IndividualPal) this.fittestIndividual());
		
		int elitismOffset = this.shouldUseElistism()?1:0;
		
		//Loop iver the population size and create new individual with crossover
		for(int index = elitismOffset; index < this.numberOfIndividuals(); index++) {
			IndividualPal newIndividual = (IndividualPal) this.tournamentSelection().crossOverWith(this.tournamentSelection());
			newPopulation.individualAtPut(index, newIndividual);
		}
		
		//Mutate population
		for(int index = elitismOffset; index < this.numberOfIndividuals(); index++)
			newPopulation.individualAt(index).mutate();

		return newPopulation;
	}

	/**
	 * Selects a couple of Individuals from the population, and returns the fittest one of that new population.
	 */
	@Override
	public IIndividual tournamentSelection() {
		PopulationPal newPopulation = new PopulationPal(Palabra);
		newPopulation.create(this.tournamentSize());
		for(int i = 0; i < this.tournamentSize(); i++) {
			int randomIndex = (int)(Math.random()*this.numberOfIndividuals());
			newPopulation.individualAtPut(i, (IndividualPal) (this.individualAt(randomIndex)));
		}
		return newPopulation.fittestIndividual();
	}

	/**
	 * Decides that the algorithms should use elitism.
	 */
	@Override
	public boolean shouldUseElistism() {
		return true;
	}
	
	/**
	 * Number of individuals in the tournamentSelection function.
	 * @return	private variable TOURNAMENT_SIZE
	 */
	public int tournamentSize(){
		return TOURNAMENT_SIZE;
	}

}
