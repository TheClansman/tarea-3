package buscadorpalabras;
import java.util.ArrayList;
import java.util.List;

import framework.IIndividual;


public class IndividualPal extends IIndividual<String>{
	
	String Palabra;
	
	/**
	 * Public Constructor
	 * @param palabra Goal word.
	 */
	public IndividualPal(String palabra){
		genes=new ArrayList<String>();
		this.Palabra=palabra;
		this.mutationRate = 0.015;
		this.uniformRate = 0.5;
		numberOfGenes=Palabra.length();
		genes=this.generateGenes();
	}
	
	/**
	 * Calculates how fit an individual is by comparing char by char the characters of that individual and the goal word.
	 * returns the score, the more score the fitter the individual.
	 */
	@Override
	public int fitness() {
		int score = 0;
		for(int i=0; i<numberOfGenes; i++){
			if(Palabra.substring(i, i+1).equals(this.geneAt(i))){
				score++;
			}
		}
		return score;
		
	}

	/**
	 * Creates a genotype, represented by a collection of random letters.
	 * return	List<String> containing one letter in each object;
	 */
	@Override
	public List<String> generateGenes() {
		String letras = "abcdefghijklmn�opqrstuvwxyz";
		List<String> answer = new ArrayList<String>();
		int index;
		for(int i=0; i<numberOfGenes; i++){
			index=(int)((Math.random())*27);
			answer.add(letras.substring(index, index+1));
		}
		return answer;
	}

	/**
	 * Crossover operation, creates a new individual selecting at random the genes from the other two.
	 * returns	new individual.
	 */
	@Override
	public IIndividual<String> crossOverWith(IIndividual<String> individual) {
		IndividualPal newIndividual = new IndividualPal(Palabra);
		for(int index = 0; index < this.numberOfGenes; index++){
			if(Math.random() < uniformRate ){
				newIndividual.geneAtPut(index, (String) this.geneAt(index));}				
			else{
				newIndividual.geneAtPut(index, (String) individual.geneAt(index));}
		}
		return newIndividual;
	}

	/**
	 * Mutate operation, changes genes from the individual from others at random.
	 */
	@Override
	public void mutate() {
		String letras = "abcdefghijklmn�opqrstuvwxyz";
		int index;
		for(int i = 0; i < this.numberOfGenes; i++)
			if(Math.random() < this.mutationRate){
				index=(int)(Math.random())*28;
				this.geneAtPut(i, letras.substring(index, index+1));
			}
				
		
	}

}
