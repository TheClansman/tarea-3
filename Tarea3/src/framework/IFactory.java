package framework;

public abstract class IFactory<T extends IIndividual> {
	
	/**
	 * Generic function to creates individuals from a Factory
	 * @param a	In case is necesary for the individual
	 * @return	a new object of the T type.
	 */
	public abstract T createIndividual(Object a);

}
