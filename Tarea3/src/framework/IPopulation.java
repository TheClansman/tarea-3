package framework;
import java.util.ArrayList;
import java.util.List;

public abstract class IPopulation<T extends IIndividual> {
	/** List of individual making the population */
	protected List<T> individuals;
	
	
	/** 
	 * Create a list of individuals
	 * @param size	number of individual to consider
	 */
	public abstract void create(int size);
	
	/**
	 * Create individuals and generate genotype for each of them
	 * @param size	size of the population
	 */
	public void createAndGenerateIndividual(int size) {
		this.create(size);
		this.generateGenotype();
	}

	/** 
	 * Generate the genotype of each individual 
	 */ 
	private void generateGenotype() {
		for(IIndividual i : individuals)
			i.generateGenes();
	}
	
	/**
	 * Create a new population, fitter to solve the problem
	 * @return a new population with better individuals
	 */
	public abstract IPopulation evolve();

	/**
	 * Pick randomly some individuals from the population, and pick the fittest individual 
	 * @return the fittest individual from the subset of the population
	 */
	public abstract IIndividual tournamentSelection();

	/**
	 * Set a particular individual
	 * @param index			position of the individual
	 * @param individual	individual to insert in the population
	 */


	/**
	 * Return an individual
	 * @param index position of the individual
	 * @return	individual at position index
	 */
	protected IIndividual individualAt(int index) {
		return individuals.get(index);
	}

	/** 
	 * Return the fittest individual of the population
	 * @return the fittest individual
	 */
	public IIndividual fittestIndividual() {
		IIndividual fittest = individuals.get(0);
		for(IIndividual current : individuals) {
			if(current.fitness() > fittest.fitness()) fittest = current;
		}
		return fittest;
	}

	/**
	 * Should an elite be used in the algorithm?  
	 */
	public abstract boolean shouldUseElistism();
	
	/**
	 * @return the size of the population
	 */
	public int numberOfIndividuals() {
		return individuals.size();
	}
	
	/**
	 * Sets the T individuals in individuals[index].
	 * @param index
	 * @param individual
	 */
	protected void individualAtPut(int index, T individual) {
		individuals.set(index, individual);
	}
}
