package framework;
import java.util.List;


public abstract class IIndividual<T> {
	
	/** Genotype of the individual */
	protected List<T> genes;

	/** Rate for the crossover operation */
	protected double uniformRate;
	
	/** Rate for the mutation operation */
	protected double mutationRate;
	
	/** Length of the genotype */
	protected int numberOfGenes;
	
	public abstract int fitness();
	
	public abstract List<T> generateGenes();
	
	public abstract IIndividual<T> crossOverWith(IIndividual<T> tournamentSelection);
	
	public abstract void mutate();
	
	/**
	 * Returns all the genes as a single String.
	 * @return
	 */
	public String genesAsString() {
		StringBuilder sb = new StringBuilder();
		for(Object i : genes) sb.append(i);
		return sb.toString();
	}
	
	/**
	 * Return the gene at index.
	 * @param index
	 * @return	Object gene.
	 */
	public Object geneAt(int index) {
		return genes.get(index);
	}
	
	/**
	 * Puts the T gene in the index.
	 * @param index
	 * @param gene
	 */
	public void geneAtPut(Integer index, T gene) {
		genes.set(index, gene);
	}

	/**
	 * Returns the protected variable List of genes of type T.
	 * @return
	 */
	public List<T> getGenes(){
		return genes;
	}

	/**
	 * Returns the protected variable uniformRate.
	 * @return
	 */
	public double getUniformRate(){
		return uniformRate;
	}
	

	/**
	 * Returns the protected variable mutationRate.
	 * @return
	 */
	public double getMutationRate(){
		return mutationRate;
	}

	/**
	 * Returns the protected variable numberOfGenes.
	 * @return
	 */
	public int getNumberOfGenes(){
		return numberOfGenes;
	}
}
